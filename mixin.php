<?php

$str = isset($_GET['v']) ? $_GET['v'] : false;

if($str) {

    $reg = '/[^(,]*(?:\([^)]+\))?[^),]*/';
    preg_match_all($reg, $str, $matches);
    $arr = array_filter(array_map('trim', $matches[0]));

    switch($arr[0]) {
        case "top":
        case "bottom":
        $position = 'x2="0%" y2="100%"';
        unset($arr[0]);
        $arr = array_values($arr);
        break;
        case "left":
        case "right":
        $position = 'x2="100%" y2="0%"';
        unset($arr[0]);
        $arr = array_values($arr);
        break;
        default:
        $position = 'x2="0%" y2="100%"';
    }

    function rgb2html($r,$g,$b) {
        if(is_array($r) && sizeof($r) == 3) {
            list($r, $g, $b) = $r;
        }
        $r = intval($r);
        $g = intval($g);
        $b = intval($b);
        $r = dechex($r<0?0:($r>255?255:$r));
        $g = dechex($g<0?0:($g>255?255:$g));
        $b = dechex($b<0?0:($b>255?255:$b));
        $color = (strlen($r) < 2?'0':'').$r;
        $color .= (strlen($g) < 2?'0':'').$g;
        $color .= (strlen($b) < 2?'0':'').$b;
        return '#'.$color;
    }

    foreach($arr as &$r){
        $r = explode(" ", $r);
        if(strpos($r[0],'gba')) {
            $rgba = str_replace(array('rgba(', ')', ' '), '', $r[0]);
            $rgba = explode(',', $rgba);
            $r[0] = rgb2html($rgba[0],$rgba[1],$rgba[2]);
            array_push($r, $rgba[3]);
        }else if(strpos($r[0],'gb')) {
            $rgb = str_replace(array('rgb(', ')', ' '), '', $r[0]);
            $rgb = explode(',', $rgb);
            $r[0] = rgb2html($rgb[0],$rgb[1],$rgb[2]);
            array_push($r, "1");
        }else {
            array_push($r, "1");
        }
    }

    header('Content-type: image/svg+xml; charset=utf-8');
    echo '<?xml version="1.0"?>';

}

?>

<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100%">
    <defs>
        <linearGradient id="linear-gradient" x1="0%" y1="0%" <?php echo $position ?>>
            <?php
                foreach($arr as $color) {
                     echo '<stop offset="'.$color[1].'" stop-color="'.$color[0].'" stop-opacity="'.$color[2].'"/>';
                }
            ?>
        </linearGradient>
    </defs>
    <rect width="100%" height="100%" fill="url(#linear-gradient)"/>
</svg>